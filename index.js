// S24 Activity:

// >>Create a variable getCube and use the exponent operator to compute
// for the cube of a number.(A cube is any number raised to 3)

const cube = 5 ** 3

// >>Using Template Literals, print out the value of the getCube variable with a message:
// The cube of < num > is…
console.log(`the cube of 5 is ${cube}`);

// >> Destructure the given array and print out a message with the full address using Template Literals.

const address = ["258", "Washington Ave NW", "California", "90011"];

const Add2 = [number, street, state, code] = address

message = `i live in ${number}, ${street}, ${state}, ${code}`
console.log(message)
    // message: I live at < details >

// >> Destructure the given object and print out a message with the details of the animal using Template Literals.

const animal = {
    name: "Lolong",
    species: "saltwater crocodile",
    weight: "1075 kgs",
    measurement: "20 ft 3 in"
};

const { name, species, weight, measurement } = animal

message2 = `${name} was a ${species}. it weighed${weight}, with a measurement of ${measurement}.`
console.log(message2)
    // message: < name > was a < species > .He weighed at < weight > with a measurement of < measurement > .

// >>
// Loop through the given array of characters using forEach, an arrow

// function and using the implicit
// return statement to print out each character

const characters = ['Ironman', 'Black Panther', 'Dr. Strange', 'Shang-Chi', 'Falcon']

characters.forEach(characters => console.log(characters))


// >>
// Create a class of a Dog and a constructor that will accept a name, age and breed as it’ s properties.

class Dog {
    constructor(name, age, breed) {
        this.name = name;
        this.age = age;
        this.breed = breed;
    };
};
let dog1 = new Dog();
dog1.name = "Buster"
dog1.age = 2
dog1.breed = "Golden Retriever"

let dog2 = new Dog();
dog2.name = "Kobe"
dog2.age = 5
dog2.breed = "Japanese Spitz"

console.log(dog1, dog2)



// >>
// Create / instantiate a 2 new object from the class Dog and console log the object